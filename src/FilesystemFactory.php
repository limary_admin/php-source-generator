<?php

namespace Sinta\Generators;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as LocalAdapter;


class FilesystemFactory
{
    public static function local($root)
    {
        return new Filesystem(new LocalAdapter($root));
    }
}