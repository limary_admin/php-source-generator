<?php

namespace Sinta\Generators\Php;


class Constant
{
    protected $name;

    public function __construct($name)
    {
        $this->name = trim($name, '\\');
    }

    public function name()
    {
        return $this->name;
    }


    public function __toString()
    {
        return $this->name;
    }
}