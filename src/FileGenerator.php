<?php

namespace Sinta\Generators;

use stdClass;
use League\Flysystem\FilesystemInterface;

class FileGenerator
{
    /**
     *
     * @var FilesystemInterface
     */
    protected $outbox;

    /**
     * @var FilesystemInterface
     */
    protected $stubbox;

    protected $context;

    public static function make($outboxRootPath, $stubboxRootPath)
    {
        if (!is_dir($outboxRootPath)) {
            mkdir($outboxRootPath, 0755, true);
        }

        $outbox = FilesystemFactory::local($outboxRootPath);
        $stubbox = FilesystemFactory::local($stubboxRootPath);

        $context = (object)[
            'outbox_root' => $outboxRootPath,
            'stubbox_root' => $stubboxRootPath,
            'directory' => null,
            'file' => null,
        ];
        return new static($outbox,$stubbox,$context);
    }


    public function __construct(FilesystemInterface $outbox, FilesystemInterface $stubbox, stdClass $context)
    {
        $this->outbox = $outbox;
        $this->stubbox = $stubbox;
        $this->context = $context;
    }

    public function directory($path, callable $callable = null)
    {
        $directory_path = $this->makePath($path);
        $this->outbox->createDir($directory_path);
        $context = clone($this->context);
        $context->directory = $directory_path;
        $sub = new static($this->outbox, $this->stubbox, $context);
        if ($callable) {
            call_user_func($callable, $sub);
        }
        return $sub;
    }


    public function sourceDirectory($path)
    {
        foreach ($this->allFiles($this->stubbox, $this->makePath($path)) as $stubbox_path) {
            if ($this->context->directory) {
                $outbox_path = substr($stubbox_path, strlen($this->context->directory) + 1);
            } else {
                $outbox_path = $stubbox_path;
            }
            $this->directory(dirname($outbox_path))->file(basename($outbox_path))->source($stubbox_path);
        }
    }


    public function templateDirectory($path, array $arguments = [])
    {
        foreach ($this->allFiles($this->stubbox, $this->makePath($path)) as $stubbox_path) {
            if ($this->context->directory) {
                $outbox_path = substr($stubbox_path, strlen($this->context->directory) + 1);
            } else {
                $outbox_path = $stubbox_path;
            }
            $this->directory(dirname($outbox_path))->file(basename($outbox_path))->template($stubbox_path, $arguments);
        }
    }

    public function keepDirectory($path, $file = '.gitkeep')
    {
        $this->directory($path)->gitKeepFile($file);
    }


    public function file($path)
    {
        $this->context->file = $this->makePath($path);
        return $this;
    }


    public function exists($path)
    {
        return $this->outbox->has($this->makePath($path));
    }

    public function blank()
    {
        $this->outbox->put($this->context->file, '');
    }

    public function text($content, array $arguments = [])
    {
        $this->outbox->put($this->context->file, $arguments ? $this->generate($content, $arguments) : $content);
    }

    public function json(array $data)
    {
        $this->outbox->put($this->context->file, json_encode($data, JSON_PRETTY_PRINT));
    }

    public function source($stub_path)
    {
        $this->outbox->put($this->context->file, $this->read($stub_path));
    }

    public function template($stub_path, array $arguments = [])
    {
        $this->outbox->put($this->context->file, $this->generate($this->read($stub_path), $arguments));
    }


    public function gitKeepFile($path = '.gitkeep')
    {
        $this->file($path)->blank();
    }


    public function phpBlankFile($path)
    {
        $this->file($path)->text('<?php'.PHP_EOL.PHP_EOL);
    }

    public function phpConfigFile($path, array $config = [], $namespace = null)
    {
        $this->file($path)->text(Php\ConfigGenerator::generateText($config, $namespace));
    }

    public function phpSourceFile($path, $source, $namespace = '')
    {
        if ($namespace) {
            $namespace = "namespace {$namespace};".PHP_EOL.PHP_EOL;
        }
        $this->file($path)->text('<?php'.PHP_EOL.PHP_EOL.$namespace.$source.PHP_EOL);
    }


    public function sourceFile($path)
    {
        $this->file($path)->source($this->makePath($path));
    }

    public function templateFile($path, array $arguments = [])
    {
        $this->file($path)->template($this->makePath($path), $arguments);
    }


    protected function makePath($path)
    {
        return $this->context->directory ? $this->context->directory.'/'.$path : $path;
    }


    /**
     * 所用文件
     *
     * @param FilesystemInterface $filesystem
     * @param $path
     * @return array
     */
    protected function allFiles(FilesystemInterface $filesystem, $path)
    {
        $files = [];
        foreach ($filesystem->listContents($path, true) as $file) {
            if ($file['type'] == 'file') {
                $files[] = $file['path'];
            }
        }
        return $files;
    }

    protected function generate($content, array $arguments)
    {
        foreach ($arguments as $name => $value) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }
            $content = preg_replace('/\{\s*\$'.$name.'\s*\}/', $value, $content);
        }
        return $content;
    }

    /**
     * 读取内容
     *
     * @param $stub_path
     * @return false|string
     */
    protected function read($stub_path)
    {
        $content = $this->stubbox->read($stub_path);
        if ($content === false) {
            throw new \InvalidArgumentException("File '$stub_path' is not found.");
        }
        return $content;
    }

}